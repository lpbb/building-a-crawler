from crawler import WebCrawler

if __name__ == "__main__":
    entry_url = "https://ensai.fr/"
    crawler = WebCrawler(entry_url)
    crawler.crawl()
    crawler.file.close()