import requests
from bs4 import BeautifulSoup
import time
from urllib.robotparser import RobotFileParser
from urllib.parse import urlparse

class WebCrawler:
    def __init__(self, entry_url):
        # Initialize variables
        self.entry_url = entry_url
        self.urls_to_crawl = [entry_url]
        self.visited_urls = []
        self.file = open("crawled_webpages.txt", "w")

    def crawl(self):
        # While loop to limit the number of URLs visited and check if there are more URLs to crawl
        while len(self.visited_urls) < 50 and len(self.urls_to_crawl) > 0:
            # Get the next URL to crawl
            current_url = self.urls_to_crawl.pop(0)

            # Make sure the URL has not been visited before
            if current_url in self.visited_urls:
                continue

            # Initialize the robot parser
            urlparsed = urlparse(current_url)
            rp = RobotFileParser()
            rp.set_url(urlparsed.scheme+"://"+urlparsed.netloc+"/robots.txt")
            rp.read()
            
            # Check if the URL is allowed according to the robots.txt file
            if not rp.can_fetch("*", current_url):
                continue

            # Download the page
            try:
                response = requests.get(current_url, timeout= 5)
            except:
                continue

            # Parse the HTML of the page
            soup = BeautifulSoup(response.text, "html.parser")

            # Write the URL to the file
            self.file.write(current_url + "\n")

            # Add the URL to the visited list
            self.visited_urls.append(current_url)

            # Find all links in the page
            links = soup.find_all("a")

            # Add the links to the URLs to crawl
            for link in links:
                if "href" in link.attrs:
                    url = link["href"]
                    if url.startswith("http"):
                        self.urls_to_crawl.append(url)

            # Wait for at least 5 seconds before downloading the next page
            time.sleep(5)


